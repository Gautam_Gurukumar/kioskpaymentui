package goodbox.ezetapmedusa.data

import io.reactivex.Observable


/**
 * Created by gautam on 14/12/17.
 */

interface TransactionAttemptData {

    fun getParentStatus(): Observable<ParentStatus>
    fun pushParentStatus(parentStatus: ParentStatus)
    fun getNotification(): Observable<String>
    fun pushNotification(message: String?)

}


enum class ParentStatus(val displayName: String) {

    INITIALIZING("Initializing"), LOGGING_IN("Logging In"),
    SEARCHING_DEVICE("Looking for device"), CONNECTING_DEVICE("Connecting to device"),
    PREPARING_DEVICE("Preparing device"), CARD_TXN("Card Transaction"),
    CHECKING_STATUS("Verifying Status"), READY("Ready"), ERROR("Error")




}
