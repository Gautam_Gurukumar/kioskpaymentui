package goodbox.ezetapmedusa.data

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject


/**
 * Created by gautam on 14/12/17.
 */
class TransactionAttemptDataImpl : TransactionAttemptData {


    val parentStatus: BehaviorSubject<ParentStatus> = BehaviorSubject.create()
    val notification: PublishSubject<String> = PublishSubject.create()
    override fun getParentStatus(): Observable<ParentStatus> {
        return parentStatus
    }

    override fun pushParentStatus(parentStatus: ParentStatus) {
        this.parentStatus.onNext(parentStatus)
    }

    override fun getNotification(): Observable<String> {
        return this.notification
    }

    override fun pushNotification(message: String?) {
        this.notification.onNext(message!!)
    }

}