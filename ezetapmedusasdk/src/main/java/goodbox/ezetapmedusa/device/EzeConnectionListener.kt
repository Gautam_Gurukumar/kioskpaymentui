package goodbox.ezetapmedusa.device

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.ezetap.medusa.sdk.Channel
import com.ezetap.medusa.sdk.ConnectionListener
import com.ezetap.medusa.sdk.DeviceConnector
import goodbox.ezetapmedusa.data.ParentStatus
import goodbox.ezetapmedusa.data.TransactionAttemptData
import io.reactivex.ObservableEmitter

/**
 * Created by gautam on 15/12/17.
 */
class EzeConnectionListener(val context: Context, val emittor: ObservableEmitter<Boolean>?, val transactionAttemptData: TransactionAttemptData) : ConnectionListener {
    override fun statusUpdate(channel: Channel?, connectionEvent: ConnectionListener.ConnectionEvent?, o: Any?) {
        var connectedToDevice: Boolean = false;
        Log.v("EzetapBl", "statusUpdate: channel = $channel : ConnectionEvent = $connectionEvent")
        if (connectionEvent == ConnectionListener.ConnectionEvent.SEARCH_COMPLETED) {

        } else if (connectionEvent == ConnectionListener.ConnectionEvent.FOUND_DEVICE) {
            Log.v("EzetapBl", "on FOUND_DEVICE ")
            if (channel == Channel.USB) {
                showToast( "Found device on usb: ")
                showToast( "Found device on usb: " + o as String)
            }
            if (channel == Channel.BT) {
                Log.v("EzetapBl", "Found device: " + o as String)
                //                prod
//                if (o.contains("31:00:27:58:86:26")) {
                //                demo
                if (o.contains("03:00:25:00:61:53")) {
                    transactionAttemptData.pushParentStatus(ParentStatus.CONNECTING_DEVICE)
                    DeviceConnector.getInstance(context).stopSearchForDevices(Channel.BT)
                    DeviceConnector.getInstance(context).connectToDevice(o, Channel.BT, this)
                }
            } else {
                Log.v("EzetapBl", "Found USB/USBA device")
            }
        } else if (connectionEvent == ConnectionListener.ConnectionEvent.NOT_CONNECTED) {
            Log.v("EzetapBl", "Not Connected")
            emittor?.onNext(false)
            emittor?.onComplete()
        } else if (connectionEvent == ConnectionListener.ConnectionEvent.DISCONNECTED) {
            Log.v("EzetapBl", "Disconnected")
            emittor?.onNext(false)
            emittor?.onComplete()
        } else if (connectionEvent == ConnectionListener.ConnectionEvent.CONNECTED) {
            Log.v("EzetapBl", "Connected")
            connectedToDevice = true;
        }
        if (connectedToDevice) {
            emittor?.onNext(true)
            emittor?.onComplete()
        }

    }


    private fun showToast(message: String) {
        Handler(Looper.getMainLooper()).post({
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        })
    }
}