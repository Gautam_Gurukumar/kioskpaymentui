package goodbox.ezetapmedusa.device

import android.os.Handler
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

/**
 * Created by gautam on 14/12/17.
 */
class FakePaymentDevice : PaymentDevice {
    override fun onFinish(): Observable<Boolean> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun doCardTransaction(d: Double): Observable<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var deviceReady = false

    override fun initializeAndKeepReady(): Observable<DeviceInitializationResult> {
//        return Observable.create(Action1<Emitter<DeviceInitializationResult>> {  },Emitter.BackpressureMode.NONE)
        return Observable.just(DeviceInitializationResult(DeviceStatus.READY, null)).delay(3000,TimeUnit.MILLISECONDS)

    }

    private fun runAfterDelay(function: () -> Unit, delay: Long) {
        Handler().postDelayed(function, delay)
    }

    override fun isDeviceReady(): Boolean {
        return deviceReady
    }

}