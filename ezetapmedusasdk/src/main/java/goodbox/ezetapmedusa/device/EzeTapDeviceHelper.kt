package goodbox.ezetapmedusa.device

import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import com.ezetap.medusa.sdk.*
import com.ezetap.utils.TimerWrapper
import com.ezetap.utils.log.JLog
import goodbox.ezetapmedusa.data.ParentStatus
import goodbox.ezetapmedusa.data.TransactionAttemptData
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by gautam on 14/12/17.
 */
class EzeTapDeviceHelper(val context: Context, val transactionAttempt: TransactionAttemptData) {


    data class EzeResult(val ezeStatus: EzeStatus, val eStatusInfo: Any?)

    private var authToken: String? = null

    fun login(): Observable<EzeResult> {
        transactionAttempt.pushParentStatus(ParentStatus.LOGGING_IN)
        return Observable.create({ emitter: ObservableEmitter<EzeResult> ->

            val loginInput = EzeLoginInput()
            /*loginInput.loginMode = EzeLoginMode.LOGIN_MODE_APPKEY
            loginInput.username = "9986400606"
            loginInput.passKey = "5784c0cb-c896-48ef-b566-a2f45665d2c2"
            loginInput.appMode = AppMode.PROD*/
            loginInput.setUsername("GOODBOX_11593851");
            loginInput.setPassKey("21a620b4-d900-444b-888d-fada94f7e18e");
            loginInput.setAppMode(AppMode.DEMO);
            val list = ArrayList<EzetapVersionData>()
            try {
                //TODO: fix following version
                list.add(EzetapVersionData("in.goodbox", "1"))

            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }


            loginInput.appInfo = list
            val status = EzeApis.getInstance(context).userLogin(loginInput) { ev, authToken, param1, param2 ->

                this.authToken = authToken;
                if (ev == EzeEvent.EZE_LOGIN_RESULT) {


                    val status = param1 as EzeStatus
                    emitter.onNext(EzeResult(status, param2))
                    emitter.onComplete()
                    //TODO: Handle ezestatusinfo
                } else if (ev == EzeEvent.EZE_EVENT_NOTIFICATION) {
                    val notification = param1 as EzeNotification
                    Log.v("Ezetap", "Notification: " + notification.getMessage(context))
                    transactionAttempt.pushNotification(notification.getMessage(context))
                } else {
                    emitter.onComplete()
                }
            }

        })

    }


    fun connectToDevice(): Observable<Boolean> {
        //TODO: Apply a timeout
        return Observable.create({ emitter: ObservableEmitter<Boolean> ->
            if (DeviceConnector.getInstance(context).isDeviceConnected) {
                emitter.onNext(true)
                emitter.onComplete()
            } else {
                transactionAttempt.pushParentStatus(ParentStatus.SEARCHING_DEVICE)
                DeviceConnector.getInstance(context).searchForDevices(Channel.BT, EzeConnectionListener(context, emitter, transactionAttempt))
            }

        })
    }

    fun prepareDevice(): Observable<EzeResult> {
        transactionAttempt.pushParentStatus(ParentStatus.PREPARING_DEVICE)
        return Observable.create({ emitter: ObservableEmitter<EzeResult> ->

            val status = EzeApis.getInstance(context).prepareDevice({ ev, authToken, param1, param2 ->
                if (ev == EzeEvent.EZE_PREPARE_RESULT) {
                    startKeepAlive()
                    emitIfSuccessAndStop(param1 as EzeStatus, emitter)

                    //Post notification if not success
                    if (EzeStatus.EZE_STATUS_SUCCESS != param1) {
                        transactionAttempt.pushNotification("EZE_PREPARE_RESULT: " + param1.getMessage(context))
                    }
                } else if (ev == EzeEvent.EZE_EVENT_NOTIFICATION) {
                    val notification = param1 as EzeNotification
                    transactionAttempt.pushNotification(notification.getMessage(context))
                } else if (ev == EzeEvent.EZE_PREPARE_PROGRESS) {
                    transactionAttempt.pushNotification("Preparing Progress: $param1/$param2")
                } else {
                    //Covering all bases
                    var param = "";
                    if (param1 != null && param1 is EzeStatus) {
                        param = param1.getMessage(context);
                    }
                    transactionAttempt.pushNotification("..." + param)
                }
            }, authToken)

            if (status != EzeStatus.EZE_STATUS_SUCCESS && status != EzeStatus.EZE_STATUS_PENDING) {
                transactionAttempt.pushNotification("Prepare device failed: " + status.getMessage(context))
            }


        })
    }

    private fun emitIfNotSuccessOrPending(status: EzeStatus, emitter: ObservableEmitter<EzeResult>) {
        if (status != EzeStatus.EZE_STATUS_SUCCESS && status != EzeStatus.EZE_STATUS_PENDING) {
            emitter.onNext(EzeResult(status, null))
            emitter.onComplete()
        }
    }

    private fun emitIfSuccessAndStop(status: EzeStatus, emitter: ObservableEmitter<EzeResult>) {
        if (EzeStatus.EZE_STATUS_SUCCESS == status) {
            emitter.onNext(EzeResult(status, null))
            emitter.onComplete()
        }
    }


    fun payCard(amount: Double): Observable<EzeResult> {
        transactionAttempt.pushParentStatus(ParentStatus.CARD_TXN)
        return Observable.create({ emitter: ObservableEmitter<EzeResult> ->
            val transactionInput = EzeTransactionInput()
            transactionInput.amount = amount
            transactionInput.type = EzeTxnType.TXN_CARD_AUTH
            val status = EzeApis.getInstance(context).transactionStart(transactionInput, { ev, authToken, param1, param2 ->
                if (ev == EzeEvent.EZE_TRANSACTION_RESULT) {
                    startKeepAlive()
                    emitIfSuccessAndStop(param1 as EzeStatus, emitter)
                    val status = param1 as EzeStatus
                    if (status == EzeStatus.EZE_STATUS_SUCCESS) {
                        var transaction: JSONObject? = null
                        try {
                            transaction = JSONObject(param2 as String)
                            Log.v("Ezetapp", "PayCard is successful:" + "\n" + "Transaction ID" + transaction.getString("txnId")
                                    + "\n" + "Receipt Url" + transaction.getString("receiptUrl"))
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    } else {

                        if (param2 != null) {
                            val statusInfo = param2 as EzeStatusInfo
                            Log.v("Ezetapp", "Transaction failed: " + statusInfo.code + ": " + statusInfo.message)
                        } else
                            Log.v("Ezetapp", "Transaction failed: " + status.getMessage(context))

                        emitter.onNext(EzeResult(EzeStatus.EZE_STATUS_NETWORK_TX_FAILURE, null))
                        emitter.onComplete()

                    }
                } else if (ev == EzeEvent.EZE_EVENT_NOTIFICATION) {
                    val notification = param1 as EzeNotification
                    transactionAttempt.pushNotification(notification.getMessage(context))
                    Log.v("Ezetapp", "Notification: " + notification.getMessage(context))

                }
            }, authToken)
            JLog.v("Ezetap", "epicSwigTransactionStart status = " + status.getMessage(context))
            if (status != EzeStatus.EZE_STATUS_SUCCESS && status != EzeStatus.EZE_STATUS_PENDING) {
                Log.v("Ezetapp", "Transaction failed: " + status.getMessage(context))
                if (status.getMessage(context).contains("status of previous transaction", true)) {
                    Log.v("Ezetapp", "Checking status of previous txn")
                    //TODO: Handle this
                    checkStatus(true).subscribe()
                }
            }

            emitIfNotSuccessOrPending(status, emitter);

        })
    }


    fun checkStatus(clearErrorCheck: Boolean): Observable<String> {
        transactionAttempt.pushParentStatus(ParentStatus.CHECKING_STATUS)
        return Observable.create({ emitter: ObservableEmitter<String> ->

            val nonce = EzeApis.getInstance(context).getNonce(authToken)
            JLog.v("Ezetap", "checkStatus nonce = " + nonce)
            val status = EzeApis.getInstance(context).checkTxnStatusStart({ ev, authToken, param1, param2 ->
                if (ev == EzeEvent.EZE_CHECK_STATUS_RESULT) {
                    val status = param1 as EzeStatus
                    if (status == EzeStatus.EZE_STATUS_SUCCESS) {

                        Log.v("Ezetapp", "checkStatus is successful")
                        try {

                            val transaction = JSONObject(param2 as String)
                            val result = JSONObject()
                            result.put("txn", transaction)

                            val responsee = JSONObject()
                            responsee.put("status", "success")
                            responsee.put("result", result)

                            val jsonObject = JSONObject()
                            jsonObject.put("response", responsee)

                            if (!clearErrorCheck) {
                                emitter.onNext(jsonObject.toString())
                                emitter.onComplete()
                            }


                            val response = JSONObject(param2)
                            if (response.has("success") && response.getBoolean("success")) {
                                Thread(Runnable { EzeApis.getInstance(context).removeNonce(authToken) }).start()
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            failedCheckStatus(emitter)
                        }

                    } else {
                        if (param2 != null) {
                            val statusInfo = param2 as EzeStatusInfo
                            if (statusInfo.code.equals("EZETAP_0000061", ignoreCase = true))
                                Thread(Runnable { EzeApis.getInstance(context).removeNonce(authToken) }).start()
                            Log.v("Ezetapp", "checkStatus failed: " + statusInfo.code + ": " + statusInfo.message)
                        } else {
                            try {
                                Log.v("Ezetapp", "checkStatus failed: " + status.getMessage(context))
                            } catch (e: Exception) {
                                Log.v("Ezetapp", "checkStatus failed. ")
                            }
                        }
                        failedCheckStatus(emitter)

                    }
                }
            }, authToken)
            JLog.v("Ezetap", "checkStatus status = " + status.getMessage(context))
            if (status != EzeStatus.EZE_STATUS_SUCCESS && status != EzeStatus.EZE_STATUS_PENDING) {
                Log.v("Ezetapp", "checkStatus failed: " + status.getMessage(context))
            }


        })
    }

    private fun failedCheckStatus(emitter: ObservableEmitter<String>) {
        emitter.onNext(getFailedJson())
        emitter.onComplete()
    }

    public fun getFailedJson(): String {
        val result = JSONObject()

        val responsee = JSONObject()
        responsee.put("status", "failure")
        responsee.put("result", result)

        val jsonObject = JSONObject()
        jsonObject.put("response", responsee)
        return jsonObject.toString()
    }


    private var timerHandle = -1
    private val KEEP_ALIVE_INTERVAL = 30 //In seconds
    private fun startKeepAlive() {
        if (timerHandle == -1) {
            DeviceConnector.getInstance(context).keepAliveDevice()
            timerHandle = TimerWrapper.getInstance().startTimer(KEEP_ALIVE_INTERVAL.toLong(), null) {
                timerHandle = -1
                startKeepAlive()
            }
        }
    }

    fun onFinish() {
        try {
            if (authToken != null) {
                EzeApis.getInstance(context).pendingOperationAbort(this.authToken)
            }
        } catch (e: Exception) {
        }
    }


}