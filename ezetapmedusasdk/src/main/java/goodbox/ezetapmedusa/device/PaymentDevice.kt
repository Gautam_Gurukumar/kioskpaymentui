package goodbox.ezetapmedusa.device

import io.reactivex.Observable


/**
 * Created by gautam on 12/12/17.
 */
interface PaymentDevice {

    fun initializeAndKeepReady(): Observable<DeviceInitializationResult>
    fun isDeviceReady(): Boolean
    fun doCardTransaction(amount: Double): Observable<String>
    fun onFinish(): Observable<Boolean>
}

data class DeviceInitializationResult(val deviceStatus: DeviceStatus, val deviceErrorState: DeviceErrorState?)

enum class DeviceStatus {
    ERROR, READY
}

enum class DeviceErrorState {
    ERROR
}
