package goodbox.ezetapmedusa.device

import android.content.Context
import com.ezetap.medusa.sdk.EzeStatus
import com.ezetap.medusa.sdk.EzeStatusInfo
import goodbox.ezetapmedusa.data.TransactionAttemptData
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableTransformer
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers

/**
 * Created by gautam on 12/12/17.
 */

class EzeTapDevice(val context: Context, transactionAttemptData: TransactionAttemptData) : PaymentDevice {
    override fun onFinish(): Observable<Boolean> {
        return Observable.create({ emitter: ObservableEmitter<Boolean> ->

            ezeTapDeviceHelper.onFinish()
            emitter.onNext(true)
            emitter.onComplete()
        })
    }


    val ezeTapDeviceHelper: EzeTapDeviceHelper = EzeTapDeviceHelper(context, transactionAttemptData)

    override fun initializeAndKeepReady(): Observable<DeviceInitializationResult> {
        //TODO: look at look at delay after "connected"
        return ezeTapDeviceHelper.login()
                .switchMap { result: EzeTapDeviceHelper.EzeResult -> ezeTapDeviceHelper.prepareDevice() }
                .map(toDeviceResult())
                .compose(ezeThread())
    }

    override fun doCardTransaction(amount: Double): Observable<String> {
        return ezeTapDeviceHelper.payCard(amount)
                .switchMap { ezeResult: EzeTapDeviceHelper.EzeResult ->
                    if (ezeResult.ezeStatus == EzeStatus.EZE_STATUS_SUCCESS) ezeTapDeviceHelper.checkStatus(false)
                    else Observable.just(ezeTapDeviceHelper.getFailedJson())
                }
                .compose(ezeThread())
    }


    private fun toDeviceResult(): Function<in EzeTapDeviceHelper.EzeResult, out DeviceInitializationResult>? {
        return Function { ezeResult: EzeTapDeviceHelper.EzeResult ->

            when (ezeResult.ezeStatus) {
                EzeStatus.EZE_STATUS_SUCCESS -> DeviceInitializationResult(DeviceStatus.READY, null)
                else -> {
                    val hasErrorInfo = ezeResult.eStatusInfo != null && ezeResult.eStatusInfo is EzeStatusInfo
                    DeviceInitializationResult(DeviceStatus.ERROR, if (hasErrorInfo) mapErrorInfo(ezeResult.eStatusInfo as EzeStatusInfo) else null)
                }
            }

        }
    }

    private fun mapErrorInfo(ezeStatusInfo: EzeStatusInfo): DeviceErrorState {
        //TODO: further mapping
        return DeviceErrorState.ERROR
    }

    override fun isDeviceReady(): Boolean {
        //TODO("not implemented")
        return false
    }


    private fun <T> ezeThread(): ObservableTransformer<T, T> {
        //TODO: IO ?
        return ObservableTransformer { observable -> observable.subscribeOn(Schedulers.io()) }
    }


}