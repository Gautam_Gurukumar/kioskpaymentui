package goodbox.ezetapmedusa

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import com.crashlytics.android.Crashlytics
import goodbox.`in`.ezetapmedusasdk.R
import goodbox.ezetapmedusa.actionHandler.ActionHandler
import goodbox.ezetapmedusa.actionHandler.ActionView
import goodbox.ezetapmedusa.data.ParentStatus
import goodbox.ezetapmedusa.data.TransactionAttemptDataImpl
import goodbox.ezetapmedusa.device.EzeTapDevice
import io.fabric.sdk.android.Fabric
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_eze_tap_card_transaction.*

class EzeTapCardTransactionActivity : AppCompatActivity(), ActionView {


    val transactionAttemptData = TransactionAttemptDataImpl()
    private lateinit var actionHandler: ActionHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_eze_tap_card_transaction)
        actionHandler = ActionHandler(transactionAttemptData, EzeTapDevice(this, transactionAttemptData), this, intent.getDoubleExtra("amount", 0.00))
        if (intent.getDoubleExtra("amount", 0.00) > 0) {
            actionHandler.onCreate()
        } else {
            finish()
        }


    }

    override fun onResume() {
        super.onResume()
        addSubscriptions()
    }

    lateinit var disposable: CompositeDisposable
    private fun addSubscriptions() {
        disposable = CompositeDisposable()
        disposable.addAll(
                transactionAttemptData.getParentStatus().compose(mainScheduler()).subscribe({ parentStatus: ParentStatus ->
                    parentStatusText.text = parentStatus.displayName
                }),
                transactionAttemptData.getNotification().compose(mainScheduler()).subscribe({ message: String ->
                    notificationText.text = message

                    click_to_see_txt.visibility = View.VISIBLE
                    question_mark.visibility = View.VISIBLE
                    dismiss_help.visibility = View.GONE
                    videoHelperLayout.visibility = View.INVISIBLE

                    if (message.contains("Please Enter Pin", true)
                            || message.contains("Please Swipe Instead", true)
                            || message.contains("or Insert", true)
                            ) {
                        notificationText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f)
                        notificationText.setTextColor(Color.BLACK)


                        val anim = AlphaAnimation(0.0f, 1.0f)
                        anim.duration = 120 //You can manage the time of the blink with this parameter
                        anim.startOffset = 20
                        anim.repeatMode = Animation.REVERSE
                        anim.repeatCount = 10
                        notificationText.startAnimation(anim)

                        progressBar.visibility = View.GONE



                        if (message.contains("Please Enter Pin", true)) {
                            sentHintHelper(R.raw.pin_entry);
                        } else if (message.contains("Please Swipe Instead", true)) {
                            sentHintHelper(R.raw.swipe_card);
                        } else if (message.contains("or Insert", true)) {
                            sentHintHelper(R.raw.chip_entry);
                        }
                        hintLayout.visibility = View.VISIBLE
                        videoHelperLayout.visibility = View.GONE
                    } else {


                        videoHelperLayout.visibility = View.GONE

                        progressBar.visibility = View.VISIBLE
                        notificationText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
                        notificationText.setTextColor(ContextCompat.getColor(this, R.color.defaultTextColor))
                    }

                })
        )
    }

    private fun <T> mainScheduler(): ObservableTransformer<T, T> {
        return ObservableTransformer { observable -> observable.observeOn(AndroidSchedulers.mainThread()) }
    }


    override fun onPause() {
        super.onPause()
        disposable.dispose()
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun sentHintHelper(videoRes: Int) {

        hintLayout.setOnClickListener({

            val visible = (videoHelperLayout.visibility) == View.VISIBLE
            // get the center for the clipping circle
            val cx = videoHelperLayout.getWidth() / 2
            val cy = videoHelperLayout.getHeight() / 2

            val cx2 = videoHelperLayout.right - hintLayout.getWidth() / 2
            val cy2 = videoHelperLayout.bottom - hintLayout.getWidth() / 2

            // get the final radius for the clipping circle
            var finalRadius = (Math.hypot(cx.toDouble(), cy.toDouble()).toFloat())
            finalRadius *= 2

            val anim = ViewAnimationUtils.createCircularReveal(videoHelperLayout, cx2, cy2, if (visible) finalRadius else 0f, if (visible) 0f else finalRadius)

            if (!visible) {


                click_to_see_txt.visibility = View.GONE
                question_mark.visibility = View.GONE
                dismiss_help.visibility = View.VISIBLE


                videoHelperLayout.visibility = View.VISIBLE

                videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + videoRes))
                videoView.setOnCompletionListener { mediaPlayer -> videoView.start() }
                videoView.start()
            } else {

                click_to_see_txt.visibility = View.VISIBLE
                question_mark.visibility = View.VISIBLE
                dismiss_help.visibility = View.GONE

                anim.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        super.onAnimationEnd(animation)
                        videoHelperLayout.visibility = View.INVISIBLE
                    }
                })
            }
            anim.start()


        })
    }

    override fun onCardTransactionResult(cardTransactionResult: String) {
        val intent = Intent()
        intent.putExtra("response", cardTransactionResult)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        actionHandler.onFinish()
    }
}
