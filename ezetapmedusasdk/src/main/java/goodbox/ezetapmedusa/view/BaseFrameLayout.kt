package goodbox.ezetapmedusa.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout

/**
 * Created by gautam on 05/12/17.
 */
abstract class BaseFrameLayout : FrameLayout {

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    fun init(attrs: AttributeSet?, defStyle: Int) {
        addView(LayoutInflater.from(context).inflate(getLayoutToInflate(), null));
        init()
    }

    abstract fun init()

    abstract fun getLayoutToInflate(): Int
}