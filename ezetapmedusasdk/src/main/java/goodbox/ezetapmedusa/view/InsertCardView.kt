package goodbox.ezetapmedusa.view

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import goodbox.`in`.ezetapmedusasdk.R


/**
 * TODO: document your custom view class.
 */
class InsertCardView : BaseFrameLayout {
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun init() {

    }

    override fun getLayoutToInflate(): Int {
        return R.layout.insert_card_demo_view;
    }

    constructor(context: Context) : super(context) {
    }


}
