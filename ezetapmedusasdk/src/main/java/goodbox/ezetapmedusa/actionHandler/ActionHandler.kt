package goodbox.ezetapmedusa.actionHandler

import goodbox.ezetapmedusa.data.ParentStatus
import goodbox.ezetapmedusa.data.TransactionAttemptData
import goodbox.ezetapmedusa.device.DeviceErrorState
import goodbox.ezetapmedusa.device.DeviceInitializationResult
import goodbox.ezetapmedusa.device.DeviceStatus
import goodbox.ezetapmedusa.device.PaymentDevice
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by gautam on 14/12/17.
 */
class ActionHandler(private val transactionAttemptData: TransactionAttemptData, val paymentDevice: PaymentDevice, val actionView: ActionView, val amount: Double) {


    fun onCreate() {
        if (paymentDevice.isDeviceReady()) {
            doCardTransaction()
        } else {
            transactionAttemptData.pushParentStatus(ParentStatus.INITIALIZING)
            paymentDevice.initializeAndKeepReady().subscribeOn(Schedulers.newThread()).subscribe({ result: DeviceInitializationResult? ->
                when (result?.deviceStatus) {
                    DeviceStatus.READY -> {
                        transactionAttemptData.pushParentStatus(ParentStatus.READY)
                        doCardTransaction()
                    }
                    DeviceStatus.ERROR -> transactionAttemptData.pushParentStatus(ParentStatus.ERROR)
                }
            })
        }

    }

    private fun doOnInitializationError(deviceErrorState: DeviceErrorState) {
        //TODO

    }

    private fun doCardTransaction() {

        paymentDevice.doCardTransaction(amount).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result: String? ->
                    if (result != null) {
                        actionView.onCardTransactionResult(result)
                    }
                })
    }

    fun onFinish() {
        paymentDevice.onFinish().subscribeOn(Schedulers.io()).subscribe()
    }


}

interface ActionView {
    fun onCardTransactionResult(cardTransactionResult: String)
}