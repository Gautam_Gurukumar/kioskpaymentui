package `in`.goodbox.kioskezetapui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        launchPayment.setOnClickListener({
            launchEzetap()
        })


    }


    private fun launchEzetap() {

        val intent = Intent()
        val amount: Double = 10.0;
        intent.putExtra("amount",amount)
        intent.setClassName("in.goodbox.kioskezetapui", "goodbox.ezetapmedusa.EzeTapCardTransactionActivity")
        startActivityForResult(intent, 1001)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1001) {
            if (resultCode == RESULT_OK) {
                val result = data?.getStringExtra("response")
                paymentResult.text = result;
            }
        }

    }
}
